//@version=3

//  Multi Indicators v1
//  20 50 200 EMA/SMA, Bollinger Bands, VWAP
//  These can be turned on and off
//  I'll be adding to this multi indicator in future updates
//  RJ Custom set of indicators
//  Copyright by RJ 4/2018


study(title="Multi Indicators", shorttitle="Multi", overlay=true)
exponential = input(true, title="Exponential instead of Simple MA")
displayMa20 = input(false, title="Display 20 MA?")
displayMa50 = input(true, title="Display 50 MA?")
displayMa200 = input(true, title="Display 200 MA?")
displayMa1200 = input(true, title="Display 1200 MA?")
displayBB = input(false, title="Display Bollinger Bands?")
displayVW = input(true, title="Display VWAP?")
BBlength = input(20, minval=1,title="Bollinger Period Length")
BBmult = input(2.0, minval=0.001, maxval=50,title="Bollinger Bands Standard Deviation")
BBBasis = na
window1 = input(title='SR Lookback window 1:', type=integer, defval=8)
window2 = input(title='SR Lookback window 2:', type=integer, defval=21)

src = close

// MAs
ema20 = exponential and displayMa20 ? ema(src, 20) : na
ema50 = exponential and displayMa50 ? ema(src, 50) : na
ema200 = exponential and displayMa200 ? ema(src, 200) : na
ema1200 = exponential and displayMa1200 ? ema(src, 1200) : na

plot( ema20, color=fuchsia, style=line, title="EMA20", linewidth=1)
plot( ema50, color=blue, style=line, title="EMA50", linewidth=1)
plot( ema200, color=red, style=line, title="EMA200", linewidth=1)
plot( ema1200, color=yellow, style=line, title="EMA1200", linewidth=1)

sma20 = not(exponential) and displayMa20 ? sma(src, 20) : na
sma50 = not(exponential) and displayMa50 ? sma(src, 50) : na
sma200 = not(exponential) and displayMa200 ? sma(src, 200) : na
sma1200 = not(exponential) and displayMa1200 ? sma(src, 1200) : na

plot( sma20, color=orange, style=line, title="SMA20", linewidth=1)
plot( sma50, color=blue, style=line, title="SMA50", linewidth=1)
plot( sma200, color=red, style=line, title="SMA200", linewidth=1)
plot( sma1200, color=yellow, style=line, title="SMA1200", linewidth=1)

// Vwap
vwp = displayVW ? vwap : na
plot( vwp, color=#6495ED, style=line, title="VWAP", linewidth=2)

// BB
BBbasis = displayBB ? sma(src, BBlength) : na
BBdev = displayBB ?  BBmult * stdev(src, BBlength) : na
BBupper = displayBB ? BBbasis + BBdev : na
BBlower = displayBB ? BBbasis - BBdev : na

plot(BBbasis, color=orange, title="Bollinger Bands SMA Basis Line")
p1 = plot(BBupper, color=#008080, title="Bollinger Bands Upper Line")
p2 = plot(BBlower, color=#008080, title="Bollinger Bands Lower Line")
fill(p1, p2)


