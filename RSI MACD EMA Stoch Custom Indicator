//@version=3
////////////////////////////////////////////////////////////
// Copyright by RJ 3/2018
// Buy when:
// RSI period 3 greater than 50, Difference between MACD and
// MACD signal line is greater than 0 and also greater than that of
// previous candle, Stochastic %D line is less than 80 and greater
// than that of previous candle, Closing price greater than EMA
// period 100.
// Sell when:
// RSI period 3 less than 50, Difference between MACD and
// MACD signal line is less than 0 and also less than that of
// previous candle, Stochastic %D line is greater than 20 and less
// than that of previous candle, Closing price less than EMA
// period 100.
////////////////////////////////////////////////////////////


study(title="RSI MACD EMA Stoch Custom Indicator", shorttitle="Custom", overlay=true)

source = close
sourcePrevCandle = close[1]
lenRsi = 3
macdFastLen = 12
macdSlowLen = 26
macdSeries = 9
stochHi = 14
stochLo = 3
stochLen = 1
emaLen = 100

rsi1 = rsi(source, lenRsi)
[macdLine, signalLine, histLine] = macd(source, macdFastLen, macdSlowLen, macdSeries)
[macdLinePrev, signalLinePrev, histLinePrev] = macd(sourcePrevCandle, macdFastLen, macdSlowLen, macdSeries)
macdDiff = macdLine - signalLine
macdDiffPrev = macdLinePrev - signalLinePrev
stochk = sma(stoch(source, stochHi, stochLo, stochLen), 3)
stochd = sma(stochk, 1)
stochkPrev = sma(stoch(sourcePrevCandle, stochHi, stochLo, stochLen), 3)
stochdPrev = sma(stochkPrev, 1)
ema1 = ema(source, emaLen)

rsiBuyCondition1 = rsi1 > 50
macdBuyCondition1 = macdDiff > 0
macdBuyCondition2 = macdDiff > macdDiffPrev
stochBuyCondition1 = stochd < 80
stochBuyCondition2 = stochd > stochdPrev
emaBuyCondition1 = source > ema1

buyCondition = rsiBuyCondition1 and macdBuyCondition1 and macdBuyCondition2 and stochBuyCondition1 and stochBuyCondition2 and emaBuyCondition1
plotshape(buyCondition, color=green, style=shape.arrowup, text="Buy")

rsiSellCondition1 = rsi1 < 50
macdSellCondition1 = macdDiff < 0
macdSellCondition2 = macdDiff < macdDiffPrev
stochSellCondition1 = stochd > 20  //I removed this, preventing any sell condition from being plotted ???
stochSellCondition2 = stochd < stochdPrev
emaSellCondition = source < ema1

sellCondition = rsiSellCondition1 and macdSellCondition1 and macdSellCondition2 and stochSellCondition2 and emaSellCondition
plotshape(sellCondition, color=red, style=shape.arrowdown, text="Sell")
